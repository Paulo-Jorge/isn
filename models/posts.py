# -*- coding: utf-8 -*-

db.define_table('posts',
                Field('userId','integer', default=auth.user_id, writable=False, readable=False),
                Field('textual', 'text', label=T('Texto') ),
                Field('files', 'upload', label=T('Ficheiro') ),
                Field('imgs', 'upload', label=T('Imagem') ),
                Field('link', 'list:string', label=T('URL') ),
                Field('tags', 'list:string', label=T('Tags') ),
                #Field('tags', 'list:string', 'reference tags', label=T('Tags') ),
                Field('privacy', 'string', label=T('Privacidade') ),
                Field('dataReg', 'datetime', default=request.now, writable=False, readable=False, label=T('Data de Criação') )
                )

db.posts.textual.requires = IS_NOT_EMPTY(error_message=T('Não pode estar vazio'))
#db.posts.privacy.requires = IS_IN_SET(['All', 'Registered', 'Friends', 'Only me', 'Groups'], zero=T('Escolha uma opção...'), error_message=T('Escolha uma opção') )
#db.posts.tags.widget = SQLFORM.widgets.autocomplete(request, db.tags.tag, limitby=(0, 10), min_length=1)

#db.posts.tags.widget = SQLFORM.widgets.autocomplete(request, db.tags.tag, id_field=db.tags.id, min_length=1)