# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

# ----------------------------------------------------------------------------------------------------------------------
# this is the main application menu add/remove items as required
# ----------------------------------------------------------------------------------------------------------------------

response.menu = [
    (T('Homepage'), False, "/isn"),
    (T('Posts'), False, '#', [
            (T('New Post'), False, URL('posts', 'new')),
            (T('View Posts'), False, URL('default', 'index')),
    ]),
    (T('Groups'), False, '#', [
            (T('New Group'), False, URL('groups', 'new')),
    ]),
]

# ----------------------------------------------------------------------------------------------------------------------
# provide shortcuts for development. you can remove everything below in production
# ----------------------------------------------------------------------------------------------------------------------

if not configuration.get('app.production'):
    _app = request.application
    response.menu += [
        #(T('My Sites'), False, URL('admin', 'default', 'site')),
        (T('CV'), False, '#', [
            (T('View'), False, URL('cv', 'view')),
            (T('Update'), False, URL('cv', 'edit')),
            (T('Grammar API'), False, URL('grammar', 'view')),
        ]),
        (T('People'), False, '#', [
            (T('Friends'), False, URL('user', 'friends')),
            (T('Search People'), False, URL('user', 'all')),
        ]),
        (T('Reports'), False, URL('report', 'report')),
        (T('Admin'), False, URL('admin', 'view')),
    ]
