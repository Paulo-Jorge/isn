# -*- coding: utf-8 -*-

db.define_table('groups',
                Field('userId','integer', default=auth.user_id, writable=False, readable=False),
                Field('groupName','string', label="Group Name"),
                Field('userIds', 'list:reference auth_user', label=T('Users') ),
                Field('dataReg', 'datetime', default=request.now, writable=False, readable=False, label=T('Data de Criação') )
                )
