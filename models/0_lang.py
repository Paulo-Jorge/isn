# -*- coding: utf-8 -*-
#Translation: if PT or EN depending if there is a last arg en or not
if request.args:
    if request.args[-1] == "en":
        session.ling = "en"
        ling = 'en'
    else:
        session.ling = "pt"
        ling = 'pt'
else:
    session.ling = "en"
    ling = 'en'
T.current_languages = []
T.force(ling)

from gluon.custom_import import track_changes
track_changes(True)
