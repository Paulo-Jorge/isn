# -*- coding: utf-8 -*-

db.define_table('projects',
Field('user_id', 'integer', default=auth.user_id, writable=False, readable=False),
Field('title', 'string', label=T("Designação")+'*'),
Field('date_start', 'date', label=T("Data de início")+'*', comment="Data no formato YYYY-MM-DD, p.e. 2018-07-30. Ano obrigatório, dia e mês opcionais, apenas para ordenação - colocar 01 se não souber o dia ou mês"),
Field('tipo', 'string', label=T("Tipo")+'*', default='Coordenador'),
Field('project', 'text', label=T("Descrição"), comment="(P.e.: coordenador(a), instituição de acolhimento, website, referência/financiamento, etc.)"),
Field('date_end', 'date', label=T("Data de conclusão"), comment="Não preencher se ainda em curso"),
Field('date_reg','datetime', default=request.now, writable=False, readable=False)
)
#db.projects.title.requires = IS_NOT_EMPTY(error_message=T('Não pode estar vazio'))
db.projects.date_start.requires = IS_EMPTY_OR(IS_DATE(format='%Y-%m-%d', error_message='must be YYYY-MM-DD!'))
#db.projects.project.requires = IS_LENGTH(1500)
db.projects.tipo.requires = IS_EMPTY_OR(IS_IN_SET(['Coordenador', 'Membro']))
db.projects.date_end.requires = IS_EMPTY_OR(IS_DATE(format='%Y-%m-%d', error_message='must be YYYY-MM-DD!'))

db.define_table('networks',
Field('user_id', 'integer', default=auth.user_id, writable=False, readable=False),
Field('network', 'string', label=T("Designação")+'*'),
Field('date_start', 'date', label=T("Início de funcionamento")+'*', comment="Data no formato YYYY-MM-DD, p.e. 2018-07-30. Ano obrigatório, dia e mês opcionais, apenas para ordenação - colocar 01 se não souber o dia ou mês"),
Field('network_info', 'text', label=T("Breve descrição")),
Field('network_website', 'string', label=T("Website URL")),
Field('date_reg','datetime', default=request.now, writable=False, readable=False)
)
#db.networks.network.requires = IS_NOT_EMPTY(error_message=T('Não pode estar vazio'))
db.networks.date_start.requires = IS_EMPTY_OR(IS_DATE(format='%Y-%m-%d', error_message='must be YYYY-MM-DD!'))
#db.networks.network_info.requires = IS_LENGTH(1500)

db.define_table('editorial_boards',
Field('user_id', 'integer', default=auth.user_id, writable=False, readable=False),
Field('journal_name', 'string', label=T("Título")+'*'),
Field('date_start', 'date', label=T("Desde (data de início)")+'*', comment="Data no formato YYYY-MM-DD, p.e. 2018-07-30. Ano obrigatório, dia e mês opcionais, apenas para ordenação - colocar 01 se não souber o dia ou mês"),
Field('journal_url', 'string', label="Website URL"),
Field('date_end', 'date', label="Até (data de fim)", comment="(Não preencher se ainda em curso)"),
Field('date_reg','datetime', default=request.now, writable=False, readable=False)
)
#db.editorial_boards.journal_name.requires = IS_NOT_EMPTY(error_message=T('Não pode estar vazio'))
db.editorial_boards.date_start.requires = IS_EMPTY_OR(IS_DATE(format='%Y-%m-%d', error_message='must be YYYY-MM-DD!'))
db.editorial_boards.date_end.requires = IS_EMPTY_OR(IS_DATE(format='%Y-%m-%d', error_message='must be YYYY-MM-DD!'))

db.define_table('scientific_committee',
Field('user_id', 'integer', default=auth.user_id, writable=False, readable=False),
Field('journal_name', 'string', label=T("Título")+'*'),
Field('date_start', 'date', label=T("Desde (data de início)")+'*', comment="Data no formato YYYY-MM-DD, p.e. 2018-07-30. Ano obrigatório, dia e mês opcionais, apenas para ordenação - colocar 01 se não souber o dia ou mês"),
Field('tipo', 'string', label=T("Tipo")+'*', default='Congresso'),
Field('journal_url', 'string', label="Website URL"),
Field('date_end', 'date', label="Até (data de fim)", comment="(Não preencher se ainda em curso)"),
Field('date_reg','datetime', default=request.now, writable=False, readable=False)
)
#db.scientific_committee.journal_name.requires = IS_NOT_EMPTY(error_message=T('Não pode estar vazio'))
db.scientific_committee.date_start.requires = IS_EMPTY_OR(IS_DATE(format='%Y-%m-%d', error_message='must be YYYY-MM-DD!'))
db.scientific_committee.tipo.requires = IS_EMPTY_OR(IS_IN_SET(['Congresso', 'Revista']))
db.scientific_committee.date_end.requires = IS_EMPTY_OR(IS_DATE(format='%Y-%m-%d', error_message='must be YYYY-MM-DD!'))


db.define_table('other_info',
Field('user_id', 'integer', default=auth.user_id, writable=False, readable=False),
Field('date_end', 'date', label="Data", default=request.now, comment="(Data de realização para ordenação - p.e. 2018-07-30; colocar 2018-01-01 se não souber o dia ou mês)"),
Field('info_entry', 'text', label=T("Outra informação")+'*'),
Field('date_reg','datetime', default=request.now, writable=False, readable=False)
)
#db.other_info.info_entry.requires = [IS_NOT_EMPTY(error_message=T('Não pode estar vazio')), IS_LENGTH(1000)]
#db.other_info.info_entry.requires = IS_LENGTH(1000)

db.define_table('thesis',
Field('user_id', 'integer', default=auth.user_id, writable=False, readable=False),
Field('thesis_title', 'string', label=T("Título")+'*'),
Field('thesis_student', 'string', label=T("Nome do autor(a)")+'*'),
Field('thesis_type', 'string', label=T("Tipo")+'*'),
Field('date_start', 'date', label=T("Início")+'*', comment="Data no formato YYYY-MM-DD, p.e. 2018-07-30. Ano obrigatório, dia e mês opcionais, apenas para ordenação - colocar 01 se não souber o dia ou mês"),
Field('advisor_type', 'string', default='Orientador', label=T("Orientador/Co-orientador")+'*'),
Field('orientador', 'string', label="Orientador"+'*', writable=False, readable=False),
Field('coorientador', 'string', label="Co-orientador", writable=False, readable=False),
Field('date_end', 'date', label="Fim", comment="(Não preencher se não estiver concluído)"),
Field('thesis_course', 'string', label="Programa de estudos"),
Field('date_reg','datetime', default=request.now, writable=False, readable=False)
)
#db.thesis.thesis_title.requires = IS_NOT_EMPTY(error_message=T('Não pode estar vazio'))
#db.thesis.thesis_student.requires = IS_NOT_EMPTY(error_message=T('Não pode estar vazio'))
db.thesis.date_start.requires = IS_EMPTY_OR(IS_DATE(format='%Y-%m-%d'))
db.thesis.date_end.requires = IS_EMPTY_OR(IS_DATE(format='%Y-%m-%d', error_message='must be YYYY-MM-DD!'))
#db.thesis.orientador.requires = IS_NOT_EMPTY(error_message=T('Não pode estar vazio'))
#db.thesis.date_end.requires = IS_NOT_EMPTY(error_message=T('Não pode estar vazio'))
db.thesis.thesis_type.requires = IS_EMPTY_OR(IS_IN_SET(['Dissertação de Mestrado', 'Tese de Doutoramento', 'Projeto de Pós-Doutoramento']))
db.thesis.advisor_type.requires = IS_EMPTY_OR(IS_IN_SET(['Orientador', 'Co-orientador']))

db.define_table('organization_of_events',
Field('user_id', 'integer', default=auth.user_id, writable=False, readable=False),
Field('event_title', 'string', label=T("Título")+'*'),
Field('event_place', 'string', label=T("Local")+'*'),
Field('date_start', 'date', label=T("Início")+'*', comment="Data no formato YYYY-MM-DD, p.e. 2018-07-30"),
Field('date_end', 'date', label=T("Fim")+'*', comment="Data no formato YYYY-MM-DD, p.e. 2018-07-30"),
Field('event_type', 'string', label=T("Tipo de evento")+'*'),
Field('organizador', 'string', label="Organizador(es)"),
Field('event_website', 'string', label="Website URL"),
Field('date_reg','datetime', default=request.now, writable=False, readable=False)
)
#db.organization_of_events.event_title.requires = IS_NOT_EMPTY(error_message=T('Não pode estar vazio'))
#db.organization_of_events.event_place.requires = IS_NOT_EMPTY(error_message=T('Não pode estar vazio'))
db.organization_of_events.date_start.requires = IS_EMPTY_OR(IS_DATE(format='%Y-%m-%d', error_message='must be YYYY-MM-DD!'))
db.organization_of_events.date_end.requires = IS_EMPTY_OR(IS_DATE(format='%Y-%m-%d', error_message='must be YYYY-MM-DD!'))
db.organization_of_events.event_type.requires = IS_EMPTY_OR(IS_IN_SET(['Colóquio/Congresso', 'Seminário', 'Aula aberta', 'Escola de Verão', 'Curso breve/Workshop']))

db.define_table('scientific_meetings',
Field('user_id', 'integer', default=auth.user_id, writable=False, readable=False),
Field('event_info', 'text', label=T("Descrição")+'*', comment="(Ex. de formatação: Autor(es), 'Título da comunicação'. In 'Título do evento', data, local do evento (instituição, cidade, país) [url])"),
#Field('event_title', 'string', label="Event title", comment="*"),
#Field('event_paper', 'string', label="Paper title", comment="*"),
#Field('event_country', 'string', label="Country", comment="*"),
#Field('event_city', 'string', label="City", comment="*"),
#Field('event_university', 'string', label="University", comment="*"),
Field('date_start', 'date', label=T("Data da comunicação")+'*', comment="Data no formato YYYY-MM-DD, p.e. 2018-07-30"),
#Field('date_end', 'date', label="Ending date", comment="*"),
Field('event_type', 'string', label=T("Tipo de evento")+'*'),
Field('invite', 'string', label=T("Por convite?")+'*'),
Field('int_nac', 'string', label=T("Nacional/Internacional")+'*', writable=False, readable=False),
#Field('event_website', 'string', label="Website"),
Field('date_reg','datetime', default=request.now, writable=False, readable=False)
)
#db.scientific_meetings.event_title.requires = IS_NOT_EMPTY(error_message=T('Não pode estar vazio'))
#db.scientific_meetings.event_country.requires = IS_NOT_EMPTY(error_message=T('Não pode estar vazio'))
#db.scientific_meetings.event_city.requires = IS_NOT_EMPTY(error_message=T('Não pode estar vazio'))
#db.scientific_meetings.event_university.requires = IS_NOT_EMPTY(error_message=T('Não pode estar vazio'))
#db.scientific_meetings.event_info.requires = IS_NOT_EMPTY(error_message=T('Não pode estar vazio'))
db.scientific_meetings.date_start.requires = IS_EMPTY_OR(IS_DATE(format='%Y-%m-%d', error_message='must be YYYY-MM-DD!'))
db.scientific_meetings.date_start.requires = IS_EMPTY_OR(IS_EMPTY_OR(IS_DATE(format='%Y-%m-%d', error_message='must be YYYY-MM-DD!')))
#db.scientific_meetings.date_end.requires = IS_NOT_EMPTY(error_message=T('Não pode estar vazio'))
#db.scientific_meetings.date_end.requires = IS_NOT_EMPTY(error_message=T('Não pode estar vazio'))
db.scientific_meetings.event_type.requires = IS_EMPTY_OR(IS_IN_SET(['Colóquio/Congresso', 'Seminário', 'Aula aberta', 'Escola de Verão', 'Curso breve/Workshop']))
db.scientific_meetings.int_nac.requires = IS_EMPTY_OR(IS_IN_SET(['Nacional', 'Internacional']))
db.scientific_meetings.invite.requires = IS_EMPTY_OR(IS_IN_SET(['Sim', 'Não']))

db.define_table('publications',
Field('user_id', 'integer', default=auth.user_id, writable=False, readable=False),
Field('cite', 'text', label=T("Referência bibliográfica (estilo APA)")+'*', comment='(Indicar se in press)'),
Field('date_pub', 'date', label=T("Data de publicação")+'*', comment="Data no formato YYYY-MM-DD, p.e. 2018-07-30. Ano obrigatório, dia e mês opcionais, apenas para ordenação - colocar 01 se não souber o dia ou mês"),
Field('pub_type', 'string', label=T("Tipo de publicação")+'*'),
Field('index_scopus', 'string', label=T("Indexado na SCOPUS?"), comment='Inserir link se indexado'),
Field('index_webs', 'string', label=T("Indexado na Web of Science?"), comment='Inserir link se indexado'),
Field('index_scielo', 'string', label=T("Indexado na SciELO?"), comment='Inserir link se indexado'),
Field('peerreview', 'string', label=T("Peer review?")+'*', default="No"),
Field('int_nac', 'string', label=T("Nacional/Internacional")+'*', default="Nacional"),
Field('repositorium', 'string', label=T("URL do RepositoriUM")),
Field('in_press', 'string', label=T("In Press?")+'*', default="No", comment="*Marcar Yes se ainda não publicado"),
Field('date_reg','datetime', default=request.now, writable=False, readable=False)
)
#db.publications.cite.requires = [IS_NOT_EMPTY(error_message=T('Não pode estar vazio')), IS_LENGTH(1000)]
#db.publications.date_pub.requires = [IS_NOT_EMPTY(error_message=T('Não pode estar vazio')), IS_DATE(format='%Y-%m-%d', error_message='must be YYYY-MM-DD!')]
db.publications.int_nac.requires = IS_EMPTY_OR(IS_IN_SET(['Nacional', 'Internacional']))
db.publications.pub_type.requires = IS_EMPTY_OR(IS_IN_SET(['Livro', 'Livro de atas', 'Capítulo de livro', 'Artigo em revista', 'Artigo em livro de atas', 'Trabalho de natureza artística', 'Outro']))
db.publications.in_press.requires = IS_EMPTY_OR(IS_IN_SET(['Yes', 'No']))
db.publications.peerreview.requires = IS_EMPTY_OR(IS_IN_SET(['Yes', 'No']))