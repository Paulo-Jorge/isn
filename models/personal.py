# -*- coding: utf-8 -*-

"""db.define_table('profiles',
                Field('userId','integer', default=auth.user_id, writable=False, readable=False),
                Field('userType', 'string', label=T('Tipo') ),
                Field('userDegree', 'string', label=T('Grau Académico') ),
                Field('', 'string', label=T('Grau Académico') ),
                #Field('escola', 'string', label=T('Universidade') ),
                Field('aniv', 'date', label=T('Data de Nascimento') ),
                Field('city', 'string',label=T('Cidade de residência') ),
                Field('dataReg', 'datetime', default=request.now, writable=False, readable=False, label=T('Data de Criação') )
                )

db.profiles.userType.requires = IS_IN_SET([T('Aluno'), T('Professor')], zero=T('Escolha uma opção...'), error_message=T('Escolha uma opção') )
db.profiles.userDegree.requires = IS_IN_SET([T('Licenciatura'), T('Mestrado'), T('Doutoramento'), T('Agregação'), T('Outro')], zero=T('Escolha uma opção...'), error_message=T('Escolha uma opção') )
"""

db.define_table('personal',
	Field('userId','integer', default=auth.user_id, writable=False, readable=False),
    Field('studies_category', 'string', label=T("Grau académico")),
    Field('professional_category', 'string', label=T("Categoria profissional")),
    Field('cvitae', 'string', label=T("CiênciaVitae"), comment='E.g. EC1F-67C9-15E7' ),
    Field('orcid', 'string', label=T("ORCID"), comment='E.g. 0000-0001-5629-9556' ),
    Field('scopus', 'string', label=T("SCOPUS"), comment='E.g. 36903250800' ),
    Field('address', 'text', default="", label=T('Endereço institucional') ),
    Field('phone', 'string', label=T('Telefone') ),
    Field('homepage', 'list:string', label=T("Website(s)")),
    Field('photo', 'upload', label=T("Foto"), comment="Will be rezised to 150x193px"),
    Field('bionote', 'text', label=T("Biografia")),
    Field('interests', 'list:string', label=T("Interesses de investigação")),
    Field('profissional', 'list:string', label=T("Experiência Profissional")),
    Field('education', 'list:string', label=T("Habilitações académicas")),
    Field('appointments', 'list:string', label=T("Cargos")),
    Field('friends', 'list:string', writable=False, readable=False),
    Field('inv_group', 'string', writable=False, readable=False)
    )
#, writable=False, readable=False

db.personal.studies_category.requires = IS_EMPTY_OR(IS_IN_SET(['Licenciatura','Mestrado','Doutoramento', 'Agregação']))
#db.personal.professional_category.requires = IS_EMPTY_OR(IS_IN_SET(['Prof. Auxiliar', 'Prof. Auxiliar com agregação', 'Prof. Associado', 'Prof. Associado com agregação', 'Prof. Catedrático', 'Investigador (com bolsa)', 'Investigador (sem bolsa)', 'Leitor', 'Professor Adjunto', 'Investigador Contratado', 'Prof. Associado aposentado', 'Prof.Catedrático aposentado']))
db.personal.bionote.requires = IS_EMPTY_OR(IS_LENGTH(3000))