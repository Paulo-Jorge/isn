# -*- coding: utf-8 -*-


def report():
    YEAR = request.now.year
    if request.vars["year"]:
        YEAR = request.vars["year"]

    RESEARCH_GROUP = ''
    groups = ['EHUM2M', 'GALABRA-UMINHO', 'GAPS','GELA', 'GHD', 'GIARTES', 'GRUPO2I', 'NETCULT', 'LTE', 'PLP', 'PRADIC']
    research_types = ['books', 'book_proceedings', 'papers', 'chapters', 'proceedings', 'artistic', 'others']

    #table = SQLFORM.grid(db.auth_user.id>0, orderby=~db.auth_user.id, buttons_placement="left", sortable=True, csv=True, searchable=True, create=False, paginate=300, deletable=False, editable=False, ignore_rw=False)

    if RESEARCH_GROUP:
        pass
    else:
        publications = db(db.publications.date_pub.year()==YEAR).select(orderby=~db.publications.date_pub)
        #pubs_by_group = db( (db.publications.user_id == db.auth_user.id ) & (db.auth_user.auth_user == "GAPS") ).select()

    return locals()