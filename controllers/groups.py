# -*- coding: utf-8 -*-
# try something like

def new(): 
    form = SQLFORM(db.groups)
    if form.process(session=None, formname='post_form').accepted:
        response.flash = 'form accepted'
        redirect(URL('default','index'))
    elif form.errors:
        response.flash = 'form has errors'
    else:
        response.flash = 'please fill the form'
    return dict(form=form)