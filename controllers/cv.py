# -*- coding: utf-8 -*-
import util

#@auth.requires_login()
def view():

    cv = False
    form = ""
    id = None
    if request.args and util.is_number(request.args[0]):
        id = request.args[0]
        type="other"
    #if auth.user:
    #    id = auth.user.id
    else:
        id = auth.user.id
        type="self"
        #if request.args and util.is_number(request.args[0]):
        #    id = request.args[0]

    #if request.args and util.is_number(request.args[0]):
    if id:
        #id = request.args[0]

        exist = db( db.auth_user.id==id ).count()

        if exist != 0:
            profile = db(db.personal.userId==id).select().first()
            projects = db(db.projects.user_id==id).select(orderby=~db.projects.date_start)
            publications = db(db.publications.user_id==id).select(orderby=~db.publications.date_pub)
            networks = db(db.networks.user_id==id).select(orderby=~db.networks.date_start)
            organization_of_events = db(db.organization_of_events.user_id==id).select(orderby=~db.organization_of_events.date_end)
            scientific_meetings = db(db.scientific_meetings.user_id==id).select(orderby=~db.scientific_meetings.date_start)
            thesis = db(db.thesis.user_id==id).select(orderby=~db.thesis.date_start)
            editorial_boards = db(db.editorial_boards.user_id==id).select(orderby=~db.editorial_boards.date_start)
            scientific_committee = db(db.scientific_committee.user_id==id).select(orderby=~db.scientific_committee.date_start)
            other_info = db(db.other_info.user_id==id).select(orderby=~db.other_info.date_reg)
            user = db( db.auth_user.id==id ).select(db.auth_user.email, db.auth_user.first_name, db.auth_user.last_name).first()
            email = user["email"]
            name = user["first_name"] + " " + user["last_name"]
            cv = True
            return dict(type=type, cv=cv, profile=profile, projects=projects, publications=publications, networks=networks, organization_of_events=organization_of_events, scientific_meetings=scientific_meetings, thesis=thesis, editorial_boards=editorial_boards, scientific_committee=scientific_committee, other_info=other_info, email=email, name=name)
        else:
            form = db( db.auth_user.id>0 ).select()
    else:
        form = db( db.auth_user.id>0 ).select()
    return dict(cv=cv, form=form)




@auth.requires_login()
def edit():
    dbs_list = ['projects', 'publications', 'networks', 'organization_of_events', 'scientific_meetings', 'editorial_boards', 'scientific_committee', 'other_info', 'thesis']

    dbs_labels_en = {'projects':'Projects', 'publications':'Publications', 'networks':'Networks', 'organization_of_events':'Organization of events', 'scientific_meetings': 'Participation in events', 'thesis':'Supervision of Thesis', 'editorial_boards':'Editorship', 'scientific_committee': 'Scientific committees', 'other_info':'Other info'}

    dbs_labels_pt = {'projects':'Projetos', 'publications':'Publicações', 'networks':'Redes', 'organization_of_events':'Organização de Eventos', 'scientific_meetings': 'Comunicações em Eventos', 'thesis':'Orientação de teses', 'editorial_boards':'Comissões Editoriais', 'scientific_committee': 'Comissões científicas', 'other_info':'Outra informação'}

    user = db(db.auth_user.id==auth.user.id).select().first()

    #email = user["email"]
    name = user["first_name"] + " " + user["last_name"]

    item=""
    if request.vars["item"] in dbs_list:
        item = request.vars["item"]
        if item == "projects":
            form = SQLFORM.grid(db.projects.user_id==auth.user.id, orderby=~db.projects.date_start, csv=True, searchable=True, create=True, paginate=100, deletable=True, editable=True, ignore_rw=False, user_signature=True, buttons_placement = 'left')
        elif item == "publications":
            form = SQLFORM.grid(db.publications.user_id==auth.user.id, csv=True, orderby=~db.publications.date_pub, searchable=True, create=True, paginate=100, deletable=True, editable=True, ignore_rw=False, user_signature=True, buttons_placement = 'left')
        elif item == "networks":
            form = SQLFORM.grid(db.networks.user_id==auth.user.id, csv=True, orderby=~db.networks.date_start, searchable=True, create=True, paginate=100, deletable=True, editable=True, ignore_rw=False, user_signature=True, buttons_placement = 'left')
        elif item == "organization_of_events":
            form = SQLFORM.grid(db.organization_of_events.user_id==auth.user.id, csv=True, orderby=~db.organization_of_events.date_start, searchable=True, create=True, paginate=100, deletable=True, editable=True, ignore_rw=False, user_signature=True, buttons_placement = 'left')
        elif item == "scientific_meetings":
            form = SQLFORM.grid(db.scientific_meetings.user_id==auth.user.id, csv=True, orderby=~db.scientific_meetings.date_start, searchable=True, create=True, paginate=100, deletable=True, editable=True, ignore_rw=False, user_signature=True, buttons_placement = 'left')
        elif item == "thesis":
            form = SQLFORM.grid(db.thesis.user_id==auth.user.id, csv=True, orderby=~db.thesis.date_end, searchable=True, create=True, paginate=100, deletable=True, editable=True, ignore_rw=False, user_signature=True, buttons_placement = 'left')
        elif item == "editorial_boards":
            form = SQLFORM.grid(db.editorial_boards.user_id==auth.user.id, csv=True, orderby=~db.editorial_boards.date_start, searchable=True, create=True, paginate=100, deletable=True, editable=True, ignore_rw=False, user_signature=True, buttons_placement = 'left')
        elif item == "scientific_committee":
            form = SQLFORM.grid(db.scientific_committee.user_id==auth.user.id, csv=True, orderby=~db.scientific_committee.date_start, searchable=True, create=True, paginate=100, deletable=True, editable=True, ignore_rw=False, user_signature=True, buttons_placement = 'left')
        elif item == "other_info":
            form = SQLFORM.grid(db.other_info.user_id==auth.user.id, csv=True, orderby=~db.other_info.date_reg, searchable=True, create=True, paginate=100, deletable=True, editable=True, ignore_rw=False, user_signature=True, buttons_placement = 'left')
        return locals()
    return dict(dbs_list=dbs_list, dbs_labels_en=dbs_labels_en, dbs_labels_pt=dbs_labels_pt, item=item, user=user, name=name)

@auth.requires_login()
def personal():
    #if if request.args and util.is_number(request.args[0])::
    #    id = request.args[0]
    #else:
    #    id= auth.user.id


    confirmado=False

    #profile = db(db.personal.userId==auth.user.id).select()
    recordId = db(db.personal.userId==auth.user.id).select().first()
    profile = SQLFORM(db.personal, recordId.id, showid=False)

    if profile.process().accepted:
        util.img_thumb(profile.vars.photo, (150, 193))
        redirect( URL('cv','edit') )
    elif profile.errors:
        response.flash = 'form has errors'
    return dict(profile=profile,confirmado=confirmado)