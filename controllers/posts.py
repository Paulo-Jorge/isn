# -*- coding: utf-8 -*-

@auth.requires_login()
def post():
    form_entry = SQLFORM(db.entry)
    if form_entry.process().accepted:
        session.flash = T('Adicionado')
        redirect(URL('default','gallery'))
    elif form_entry.errors:
        session.flash = T('Erro')
    return dict(form_entry = form_entry)

def addFile():
    data = request.vars.data
    status=""
    form = SQLFORM(db.files)
    if form.process().accepted:
        session.flash = T('Adicionado')
        #redirect(URL('default','gallery'))
        status="ok"
    elif form.errors:
        session.flash = T('Erro')
        status="error"
    #return dict(form_entry = form_entry)
    return "ajaxProcess(%s);" % repr(status)

def new():
    type="self"
    username = auth.user.first_name + " " + auth.user.last_name
    form = SQLFORM(db.posts)
    if form.process(session=None, formname='post_form').accepted:
        response.flash = 'form accepted'
        redirect(URL('default','index'))
    elif form.errors:
        response.flash = 'form has errors'
    else:
        response.flash = 'please fill the form'
    # Note: no form instance is passed to the view
    return dict(form=form,username=username, type=type)