# -*- coding: utf-8 -*-

import util

def index():

    if auth.user:
        if request.args and util.is_number(request.args[0]):
            type="other"
            posts = db(db.posts.userId == request.args[0] ) .select(orderby=~db.posts.dataReg)
            user = db(db.auth_user.id==request.args[0]).select().first()
            username = user.first_name + " " + user.last_name
            return dict(posts=posts, username=username, type=type, id=request.args[0])

        else:
            type="self"
            personalFriends = db(db.personal.userId == auth.user.id).select(db.personal.friends).first()
            friendsIds = personalFriends.friends
            posts = db((db.posts.userId == auth.user.id) & (db.posts.userId in [1,2,3])) .select(orderby=~db.posts.dataReg)
            username = auth.user.first_name + " " + auth.user.last_name

            form = SQLFORM(db.posts)
            if form.process(session=None, formname='post_form').accepted:
                response.flash = 'form accepted'
                redirect(URL('default','index'))
            elif form.errors:
                response.flash = 'form has errors'
            else:
                #response.flash = 'please fill the form'
                pass
            # Note: no form instance is passed to the view
            return dict(form=form, posts=posts, username=username, type=type)
    else:
        return dict()

def files():
    print("files enter")
    status=""

    form = SQLFORM(db.files)
    if form.process(session=None, formname='post_form').accepted:
        response.flash = 'form accepted'
        status="Form processed"
    elif form.errors:
        status="Form has errors"
        response.flash = 'form has errors'
    else:
        status="empty"
        response.flash = 'please fill the form'
    """form.process()
    if request.ajax:
        if form.accepted:
            status="Form processed"
            print("files process")
            response.flash = 'form accepted'
        else:
            status="Form has errors"
            print(form)
            response.flash = form.errors"""

    # Note: no form instance is passed to the view
    return dict(status=status)

def fileUpload():
            response.files.append(URL(r=request, c='static', f='fileuploader.css'))
            for r in request.vars:
             if r=="qqfile":
                filename=request.vars.qqfile
                # process the file here
             db.files.insert(fileUp=db.files.fileUp.store(request.body,filename))                                 
            return response.json({'success':'true'})


@auth.requires_login()
def api_get_user_email():
    if not request.env.request_method == 'GET': raise HTTP(403)
    return response.json({'status':'success', 'email':auth.user.email})


@auth.requires_membership('admin') 
def grid():
    response.view = 'generic.html'
    tablename = request.args(0)
    if not tablename in db.tables: raise HTTP(403)
    grid = SQLFORM.smartgrid(db[tablename], args=[tablename], deletable=False, editable=False)
    return dict(grid=grid)


#def wiki():
#    auth.wikimenu() # add the wiki to the menu
#    return auth.wiki() 

def __createProfile(form):
    user_id = form.vars.id
    db.personal.insert(userId=user_id)

# Action for login/register/etc (required for auth)
def user():
    auth.settings.register_onaccept = __createProfile
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    http://..../[app]/default/user/bulk_register
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    also notice there is http://..../[app]/appadmin/manage/auth to allow administrator to manage users
    """
    return dict(form=auth())

# server uploaded static content
@cache.action()
def download():
    return response.download(request, db)
