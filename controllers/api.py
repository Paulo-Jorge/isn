# -*- coding: utf-8 -*-
import gluon.contrib.simplejson


@request.restful()
def tables():
    response.view = 'generic.json'

    def GET(tablename, id=None):
        #if not tablename == 'posts':
        if tablename not in ["posts", "tags", "personal", "auth_user", "projects", "networks", "editorial_boards", "scientific_committee", "other_info", "thesis", "organization_of_events", "scientific_meetings", "publications"]:
            raise HTTP(400)
        if id:
            return dict(data = db(db[tablename].id==id).select())
        else:
            return dict(data = db(db[tablename].id>0).select())

    """def GET(*args, **vars):
        users = db(db.auth_user.id>0).select()
        return gluon.contrib.simplejson.dumps(users)
    def POST(*args, **vars):
        return dict()
    def PUT(*args, **vars):
        return dict()
    def DELETE(*args, **vars):
        return dict()"""

    def POST(tablename, **fields):
        if tablename not in ["other_info"]:
            raise HTTP(400)
        #next_id = db.executesql('select max(id) from tags')[0]
        #next_id = db.tags.id.max()
        #return db[tablename].validate_and_insert(id=next_id+1, **fields)
        try:
            db[tablename].validate_and_insert(**fields)
            return({'status':'ok'})
        except:
            return({'status':'error'})

    return locals()
