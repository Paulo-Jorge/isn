**ISN**

ISN web2py app
---

## Requirements:

Necessário:

1. web2py v.18+
2. Python 2.7
3. Python2 Antlr runtime (install with: pip install antlr4-python2-runtime)
4. Python2 PIL library (pip install Pillow)

## Grammar
Gramática em moduls/grammar.g4
Ficheiros Lexer, Parser e Visitors em Python gerados com o comando:
antlr4 -Dlanguage=Python2 -visitor ISN.g4