# Generated from ISN.g4 by ANTLR 4.7.2
from antlr4 import *

# This class defines a complete generic visitor for a parse tree produced by ISNParser.

class ISNVisitor(ParseTreeVisitor):

    # Visit a parse tree produced by ISNParser#cv.
    def visitCv(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ISNParser#table.
    def visitTable(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ISNParser#personal.
    def visitPersonal(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ISNParser#projects.
    def visitProjects(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ISNParser#networks.
    def visitNetworks(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ISNParser#editorialBoards.
    def visitEditorialBoards(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ISNParser#scientificCommittee.
    def visitScientificCommittee(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ISNParser#otherInfo.
    def visitOtherInfo(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ISNParser#thesis.
    def visitThesis(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ISNParser#organizationOfEvents.
    def visitOrganizationOfEvents(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ISNParser#scientificMeetings.
    def visitScientificMeetings(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ISNParser#publications.
    def visitPublications(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ISNParser#listFormat.
    def visitListFormat(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ISNParser#date.
    def visitDate(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ISNParser#year.
    def visitYear(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ISNParser#month.
    def visitMonth(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ISNParser#day.
    def visitDay(self, ctx):
        return self.visitChildren(ctx)


