#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, json
from antlr4 import *
#from grammar.ISNLexer import ISNLexer
#from grammar.ISNParser import ISNParser
#from grammar.ISNVisitor import ISNVisitor
from ISNLexer import ISNLexer
from ISNParser import ISNParser
from ISNVisitor import ISNVisitor

#methodods úteis:
#ctx.getChildCount()
#ctx.getChild(1)
#ctx.getText()
#for child in ctx.children
#ctx.ID(n)getText() => selecciona só do elemento com ID n
#ctx.parentCtx

class VisitISN(ISNVisitor):
    def __init__(self):
        self.json = ""
        self.tables = {}

    def visitCv(self, ctx):

        #for t in self.tables:
        #    self.json += t + "-"


        #self.json.write(ctx.WORD().getText()) 
        #self.json.write("</strong> ")
        #self.json += "olllle"
        #return self.json
        return self.visitChildren(ctx)

    def visitTable(self, ctx):
        tables = []
        #for t in ctx.getChild(0):
        #    tables.append(t)
        #rule = Visit(ctx.GetChild("c"))
        #print("0===============")
        #t = ctx.getChild("personal")
        #print("><<<<<<")
        #print(ctx.getChildCount())

        return self.visitChildren(ctx)

    def visitPersonal(self, ctx):
        #print(ctx.getChildCount())
        #print(ctx.getRuleIndex())
        #print(ctx.children.getRuleIndex())
        row = self.processTable(ctx.children, "personal", ctx)

        #if "personal" not in self.tables:
        #    self.tables["personal"] = []
        #self.tables["personal"].append(row)

        #self.json += '["personal":' + row + ']'

        #return self.visitChildren(ctx)
        return self.visitChildren(ctx)

    def visitProjects(self, ctx):
        row = self.processTable(ctx.children, "projects", ctx)
        return self.visitChildren(ctx)

    def visitNetworks(self, ctx):
        row = self.processTable(ctx.children, "networks", ctx)
        return self.visitChildren(ctx)

    def visitEditorialBoards(self, ctx):
        row = self.processTable(ctx.children, "editorial_boards", ctx)
        return self.visitChildren(ctx)

    def visitScientificCommittee(self, ctx):
        row = self.processTable(ctx.children, "scientific_committee", ctx)
        return self.visitChildren(ctx)

    def visitOtherInfo(self, ctx):
        row = self.processTable(ctx.children, "other_info", ctx)
        return self.visitChildren(ctx)

    def visitThesis(self, ctx):
        row = self.processTable(ctx.children, "thesis", ctx)
        return self.visitChildren(ctx)

    def visitOrganizationOfEvents(self, ctx):
        row = self.processTable(ctx.children, "organization_of_events", ctx)
        return self.visitChildren(ctx)

    def visitScientificMeetings(self, ctx):
        row = self.processTable(ctx.children, "scientific_meetings", ctx)
        return self.visitChildren(ctx)

    def visitPublications(self, ctx):
        row = self.processTable(ctx.children, "publications", ctx)
        return self.visitChildren(ctx)

    def processTable(self, fields, tableName, ctx):
        top = True
        row = {}
        name = ""
        #print(fields)

        for field in fields:
            txt = field.getText()
            txt = txt.replace('"', '')
            txt = txt.strip()
            #if txt != ":":
            #    if top == True:
            #        row += '{"'+txt+'":'
            #        top = False
            #    else:
            #        row += '"'+txt+'"},'
            #        top = True
            if txt != ":":
                if top == True:
                    top = False
                    name = txt
                else:
                    row[name] = txt
                    top = True
        #print(ctx.getRuleIndex())

        if tableName not in self.tables:
            self.tables[tableName] = []

        self.tables[tableName].append(row)
        return row

def main(data, file=False):
    import json
    if file:
        input_stream = FileStream(data)
    else:#if string not file
        input_stream = InputStream(data.decode("utf-8"))
    lexer = ISNLexer(input_stream)
    stream = CommonTokenStream(lexer)
    parser = ISNParser(stream)
    tree = parser.cv()

    if parser.getNumberOfSyntaxErrors()!=0:
        #print("File contains {} syntax errors".format(parser.getNumberOfSyntaxErrors()))
        errors = "Document contains {} syntax errors".format(parser.getNumberOfSyntaxErrors())
        return errors


    visitor = VisitISN()

    visitor.visit(tree)

    json = json.dumps(visitor.tables)
    return dict(json=json, tables=visitor.tables)

 
if __name__ == '__main__':
    main(sys.argv[1], file=True)