# Generated from ISN.g4 by ANTLR 4.7.2
from antlr4 import *

# This class defines a complete listener for a parse tree produced by ISNParser.
class ISNListener(ParseTreeListener):

    # Enter a parse tree produced by ISNParser#cv.
    def enterCv(self, ctx):
        pass

    # Exit a parse tree produced by ISNParser#cv.
    def exitCv(self, ctx):
        pass


    # Enter a parse tree produced by ISNParser#table.
    def enterTable(self, ctx):
        pass

    # Exit a parse tree produced by ISNParser#table.
    def exitTable(self, ctx):
        pass


    # Enter a parse tree produced by ISNParser#personal.
    def enterPersonal(self, ctx):
        pass

    # Exit a parse tree produced by ISNParser#personal.
    def exitPersonal(self, ctx):
        pass


    # Enter a parse tree produced by ISNParser#projects.
    def enterProjects(self, ctx):
        pass

    # Exit a parse tree produced by ISNParser#projects.
    def exitProjects(self, ctx):
        pass


    # Enter a parse tree produced by ISNParser#networks.
    def enterNetworks(self, ctx):
        pass

    # Exit a parse tree produced by ISNParser#networks.
    def exitNetworks(self, ctx):
        pass


    # Enter a parse tree produced by ISNParser#editorialBoards.
    def enterEditorialBoards(self, ctx):
        pass

    # Exit a parse tree produced by ISNParser#editorialBoards.
    def exitEditorialBoards(self, ctx):
        pass


    # Enter a parse tree produced by ISNParser#scientificCommittee.
    def enterScientificCommittee(self, ctx):
        pass

    # Exit a parse tree produced by ISNParser#scientificCommittee.
    def exitScientificCommittee(self, ctx):
        pass


    # Enter a parse tree produced by ISNParser#otherInfo.
    def enterOtherInfo(self, ctx):
        pass

    # Exit a parse tree produced by ISNParser#otherInfo.
    def exitOtherInfo(self, ctx):
        pass


    # Enter a parse tree produced by ISNParser#thesis.
    def enterThesis(self, ctx):
        pass

    # Exit a parse tree produced by ISNParser#thesis.
    def exitThesis(self, ctx):
        pass


    # Enter a parse tree produced by ISNParser#organizationOfEvents.
    def enterOrganizationOfEvents(self, ctx):
        pass

    # Exit a parse tree produced by ISNParser#organizationOfEvents.
    def exitOrganizationOfEvents(self, ctx):
        pass


    # Enter a parse tree produced by ISNParser#scientificMeetings.
    def enterScientificMeetings(self, ctx):
        pass

    # Exit a parse tree produced by ISNParser#scientificMeetings.
    def exitScientificMeetings(self, ctx):
        pass


    # Enter a parse tree produced by ISNParser#publications.
    def enterPublications(self, ctx):
        pass

    # Exit a parse tree produced by ISNParser#publications.
    def exitPublications(self, ctx):
        pass


    # Enter a parse tree produced by ISNParser#listFormat.
    def enterListFormat(self, ctx):
        pass

    # Exit a parse tree produced by ISNParser#listFormat.
    def exitListFormat(self, ctx):
        pass


    # Enter a parse tree produced by ISNParser#date.
    def enterDate(self, ctx):
        pass

    # Exit a parse tree produced by ISNParser#date.
    def exitDate(self, ctx):
        pass


    # Enter a parse tree produced by ISNParser#year.
    def enterYear(self, ctx):
        pass

    # Exit a parse tree produced by ISNParser#year.
    def exitYear(self, ctx):
        pass


    # Enter a parse tree produced by ISNParser#month.
    def enterMonth(self, ctx):
        pass

    # Exit a parse tree produced by ISNParser#month.
    def exitMonth(self, ctx):
        pass


    # Enter a parse tree produced by ISNParser#day.
    def enterDay(self, ctx):
        pass

    # Exit a parse tree produced by ISNParser#day.
    def exitDay(self, ctx):
        pass


