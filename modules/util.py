#!/usr/bin/env python
# -*- coding: utf-8 -*-
from gluon import *

#determina se na string do arg existe um número
def is_number(x):
    try:
        float(x)
        return True
    except:
        return False

def img_thumb(image, size):
    import os
    from gluon import current
    from PIL import Image
    BASE_DIR = os.path.dirname(__file__)
    PHOTOS_DIR = os.path.join(BASE_DIR, "uploads")
    #THUMBNAILS_DIR = os.path.join(BASE_DIR, "static","photos_thumbnails")
    if image:
        request = current.request
        img = Image.open(request.folder + '/uploads/' + image)
        #photo_path = os.path.join(PHOTOS_DIR, image)
        #img = Image.open(photo_path)
        img.convert('RGB')
        img.thumbnail(size, Image.ANTIALIAS)
        #img.thumbnail(size, Image.NEAREST, Image.ANTIALIAS)
        #save_path = os.path.join(THUMBNAILS_DIR, image.path.split(".")[0]+"_thumbnail."+image.path.split(".")[1])
        root, ext = os.path.splitext(image)
        img.save(request.folder + '/uploads/' + root + ext, format='JPEG', subsampling=0, quality=100)
        #img.save(photo_path, quality=100)

def is_letters_digits(x): #determina se contém apenas letras e números. Filtra todo o tipo de símbolos e palavras acentuadas.
    if re.match("^[0-9 a-z A-Z]+$", x):
        return True
    else:
        return False